# DECIDO specifics

## Notifications

Make sure to configure a valid SMTP server in the environment variables.


## Configure generic OAuth2 authentication[](#configure-generic-oauth2-authentication)


This topic describes how to configure generic OAuth2 authentication and includes [examples of setting up generic OAuth2](#examples-of-setting-up-generic-oauth2) with specific OAuth2 providers.

### Before you begin[](#before-you-begin)

To follow this guide:

*   Ensure that you have access to the Grafana docker-compose file.
*   Ensure you know how to create an OAuth2 application with your OAuth2 provider. Consult the documentation of your OAuth2 provider for more information.
*   If you are using refresh tokens, ensure you know how to set them up with your OAuth2 provider. Consult the documentation of your OAuth2 provider for more information.

### Steps[](#steps)


To integrate your OAuth2 provider with Grafana using our generic OAuth2 authentication, follow these steps:

1.  Create an OAuth2 application in your chosen OAuth2 provider.

2.  Set the callback URL for your OAuth2 app to `http://<my_grafana_server_name_or_ip>:<grafana_server_port>/login/generic_oauth`.

    Ensure that the callback URL is the complete HTTP address that you use to access Grafana via your browser, but with the appended path of `/login/generic_oauth`.

    For the callback URL to be correct, it might be necessary to set the `root_url` option in the `[server]`section of the Grafana configuration file. For example, if you are serving Grafana behind a proxy.

3.  Refer to the following table to update field values located in the `[auth.generic_oauth]` section of the Grafana docker-compose file (GF_AUTH_GENERIC_OAUTH_):


| Field | Description |
| --- | --- |
| `client_id`, `client_secret` | These values must match the client ID and client secret from your OAuth2 app. |
| `auth_url` | The authorization endpoint of your OAuth2 provider. |
| `api_url` | The user information endpoint of your OAuth2 provider. Information returned by this endpoint must be compatible with [OpenID UserInfo](https://connect2id.com/products/server/docs/api/userinfo). |
| `enabled` | Enables generic OAuth2 authentication. Set this value to `true`. |



Review the list of other generic OAuth2 [configuration options](#configuration-options) and complete them, as necessary.

4.  Optional: [Configure a refresh token](#configure-a-refresh-token):

    a. Enable `accessTokenExpirationCheck` feature toggle.

    b. Extend the `scopes` field of `[auth.generic_oauth]` section in Grafana configuration file with refresh token scope used by your OAuth2 provider.

    c. Set `use_refresh_token` to `true` in `[auth.generic_oauth]` section in Grafana configuration file.

    d. Enable the refresh token on the provider if required.

5.  [Configure role mapping](#configure-role-mapping).

6.  Optional: [Configure team synchronization](#configure-team-synchronization).

7.  Restart Grafana.

    You should now see a generic OAuth2 login button on the login page and be able to log in or sign up with your OAuth2 provider.


### Configuration options[](#configuration-options)

The following table outlines the various generic OAuth2 configuration options. You can apply these options as environment variables, similar to any other configuration within Grafana.

| Setting | Required | Description | Default |
| --- | --- | --- | --- |
| `enabled` | No | Enables generic OAuth2 authentication. | `false` |
| `name` | No | Name that refers to the generic OAuth2 authentication from the Grafana user interface. | `OAuth` |
| `icon` | No | Icon used for the generic OAuth2 authentication in the Grafana user interface. | `signin` |
| `client_id` | Yes | Client ID provided by your OAuth2 app. |   |
| `client_secret` | Yes | Client secret provided by your OAuth2 app. |   |
| `auth_url` | Yes | Authorization endpoint of your OAuth2 provider. |   |
| `token_url` | Yes | Endpoint used to obtain the OAuth2 access token. |   |
| `api_url` | Yes | Endpoint used to obtain user information compatible with [OpenID UserInfo](https://connect2id.com/products/server/docs/api/userinfo). |   |
| `auth_style` | No | Name of the [OAuth2 AuthStyle](https://pkg.go.dev/golang.org/x/oauth2#AuthStyle) to be used when ID token is requested from OAuth2 provider. It determines how `client_id` and `client_secret` are sent to Oauth2 provider. Available values are `AutoDetect`, `InParams` and `InHeader`. | `AutoDetect` |
| `scopes` | No | List of comma- or space-separated OAuth2 scopes. | `user:email` |
| `empty_scopes` | No | Set to `true` to use an empty scope during authentication. | `false` |
| `allow_sign_up` | No | Controls Grafana user creation through the generic OAuth2 login. Only existing Grafana users can log in with generic OAuth if set to `false`. | `true` |
| `auto_login` | No | Set to `true` to enable users to bypass the login screen and automatically log in. This setting is ignored if you configure multiple auth providers to use auto-login. | `false` |
| `id_token_attribute_name` | No | The name of the key used to extract the ID token from the returned OAuth2 token. | `id_token` |
| `login_attribute_path` | No | [JMESPath](http://jmespath.org/examples.html) expression to use for user login lookup from the user ID token. For more information on how user login is retrieved, refer to [Configure login](https://grafana.com/docs/grafana/latest/setup-grafana/configure-security/configure-authentication/generic-oauth/#configure-login). |   |
| `name_attribute_path` | No | [JMESPath](http://jmespath.org/examples.html) expression to use for user name lookup from the user ID token. This name will be used as the user’s display name. For more information on how user display name is retrieved, refer to [Configure display name](https://grafana.com/docs/grafana/latest/setup-grafana/configure-security/configure-authentication/generic-oauth/#configure-display-name). |   |
| `email_attribute_path` | No | [JMESPath](http://jmespath.org/examples.html) expression to use for user email lookup from the user information. For more information on how user email is retrieved, refer to [Configure email address](https://grafana.com/docs/grafana/latest/setup-grafana/configure-security/configure-authentication/generic-oauth/#configure-email-address). |   |
| `email_attribute_name` | No | Name of the key to use for user email lookup within the `attributes` map of OAuth2 ID token. For more information on how user email is retrieved, refer to [Configure email address](https://grafana.com/docs/grafana/latest/setup-grafana/configure-security/configure-authentication/generic-oauth/#configure-email-address). | `email:primary` |
| `role_attribute_path` | No | [JMESPath](http://jmespath.org/examples.html) expression to use for Grafana role lookup. Grafana will first evaluate the expression using the OAuth2 ID token. If no role is found, the expression will be evaluated using the user information obtained from the UserInfo endpoint. The result of the evaluation should be a valid Grafana role (`Viewer`, `Editor`, `Admin` or `GrafanaAdmin`). For more information on user role mapping, refer to [Configure role mapping](https://grafana.com/docs/grafana/latest/setup-grafana/configure-security/configure-authentication/generic-oauth/#configure-role-mapping). |   |
| `role_attribute_strict` | No | Set to `true` to deny user login if the Grafana role cannot be extracted using `role_attribute_path`. For more information on user role mapping, refer to [Configure role mapping](https://grafana.com/docs/grafana/latest/setup-grafana/configure-security/configure-authentication/generic-oauth/#configure-role-mapping). | `false` |
| `allow_assign_grafana_admin` | No | Set to `true` to enable automatic sync of the Grafana server administrator role. If this option is set to `true` and the result of evaluating `role_attribute_path` for a user is `GrafanaAdmin`, Grafana grants the user the server administrator privileges and organization administrator role. If this option is set to `false` and the result of evaluating `role_attribute_path` for a user is `GrafanaAdmin`, Grafana grants the user only organization administrator role. For more information on user role mapping, refer to [Configure role mapping](https://grafana.com/docs/grafana/latest/setup-grafana/configure-security/configure-authentication/generic-oauth/#configure-role-mapping). | `false` |
| `skip_org_role_sync` | No | Set to `true` to stop automatically syncing user roles. This will allow you to set organization roles for your users from within Grafana manually. | `false` |
| `groups_attribute_path` | No | [JMESPath](http://jmespath.org/examples.html) expression to use for user group lookup. Grafana will first evaluate the expression using the OAuth2 ID token. If no groups are found, the expression will be evaluated using the user information obtained from the UserInfo endpoint. The result of the evaluation should be a string array of groups. |   |
| `allowed_groups` | No | List of comma- or space-separated groups. The user should be a member of at least one group to log in. If you configure `allowed_groups`, you must also configure `groups_attribute_path`. |   |
| `allowed_organizations` | No | List of comma- or space-separated organizations. The user should be a member of at least one organization to log in. |   |
| `allowed_domains` | No | List comma- or space-separated domains. The user should belong to at least one domain to log in. |   |
| `team_ids` | No | String list of team IDs. If set, the user must be a member of one of the given teams to log in. If you configure `team_ids`, you must also configure `teams_url` and `team_ids_attribute_path`. |   |
| `team_ids_attribute_path` | No | The [JMESPath](http://jmespath.org/examples.html) expression to use for Grafana team ID lookup within the results returned by the `teams_url` endpoint. |   |
| `teams_url` | No | The URL used to query for team IDs. If not set, the default value is `/teams`. If you configure `teams_url`, you must also configure `team_ids_attribute_path`. |   |
| `tls_skip_verify_insecure` | No | If set to `true`, the client accepts any certificate presented by the server and any host name in that certificate. _You should only use this for testing_, because this mode leaves SSL/TLS susceptible to man-in-the-middle attacks. | `false` |
| `tls_client_cert` | No | The path to the certificate. |   |
| `tls_client_key` | No | The path to the key. |   |
| `tls_client_ca` | No | The path to the trusted certificate authority list. |   |
| `use_pkce` | No | Set to `true` to use [Proof Key for Code Exchange (PKCE)](https://datatracker.ietf.org/doc/html/rfc7636). Grafana uses the SHA256 based `S256` challenge method and a 128 bytes (base64url encoded) code verifier. | `false` |
| `use_refresh_token` | No | Set to `true` to use refresh token and check access token expiration. The `accessTokenExpirationCheck` feature toggle should also be enabled to use refresh token. | `false` |

### Configure login[](#configure-login)

Grafana can resolve a user’s login from the OAuth2 ID token or user information retrieved from the OAuth2 UserInfo endpoint. Grafana looks at these sources in the order listed until it finds a login. If no login is found, then the user’s login is set to user’s email address.

Refer to the following table for information on what to configure based on how your Oauth2 provider returns a user’s login:

| Source of login | Required configuration |
| --- | --- |
| `login` or `username` field of the OAuth2 ID token. | N/A |
| Another field of the OAuth2 ID token. | Set `login_attribute_path` configuration option. |
| `login` or `username` field of the user information from the UserInfo endpoint. | N/A |
| Another field of the user information from the UserInfo endpoint. | Set `login_attribute_path` configuration option. |

For further information see the documentation here:
https://grafana.com/docs/grafana/latest/setup-grafana/configure-security/configure-authentication/generic-oauth/

## Deployment

Once everything is correctly set up you can use docker-compose to deploy grafana.

```console
docker-compose up -d
```

## Integration

Generate a global API key and store it safely.  
