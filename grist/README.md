## DECIDO Specifics

The `docker-compose.yml` file is located in this folder. You will need to adjust the environment variables to your needs.

### Grist

The docker compose file contains the Grist app deployment, which is accompanied by a PostgreSQL database.

Make sure to adjust the DB username and password as well as the SAML variables.

## Integration with SAML

You need to create a Grist-specific SAML client:

*   The **Client ID** in Keycloak should be `https://<grist-host>/saml/metadata.xml`
*   Keycloak needs to know where to redirect after login/logout
    *   Allow redirecting after login by setting the **Valid Redirect URIs** to `https://<grist-host>/*`
    *   Enable redirecting after logout by setting the **Logout Service Redirect Binding URL** (under **Fine Grain SAML Endpoint Configuration**)
*   Protocol mappers are needed; the builtin mappers work, but their SAML attribute names should be changed to work with [SAML2-js](https://github.com/Clever/saml2):
    *   **givenName:** `http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname`
    *   **surname:** `http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname`
    *   **email:** `http://schemas.xmlsoap.org/ws/2005/05/identity/claims/emailaddress`

Grist needs the following information from Keycloak:

*   The SAML login/logout URL in Keycloak 17 is `https://<keycloak-host>/realms/<realm>/protocol/saml` (in Keycloak 16, it would've been `https://<keycloak-host>/auth/realms/<realm>/protocol/saml`; note the `/auth`)
*   The client's private key and certificate could be obtained from the **Installation** tab (on the client page)
*   Keycloak's server (realm) certificate could be obtained from **Realm Settings** in the **General** tab behind **SAML 2.0 Identity Provider Metadata**
*   These keys and certificates should be placed in files accessible to Grist (as indicated in [SamlConfig.ts](https://github.com/gristlabs/grist-core/blob/main/app/server/lib/SamlConfig.ts)) with appropriate PEM headers/footers:
    *   `-----BEGIN RSA PRIVATE KEY-----`/`-----END RSA PRIVATE KEY-----`
    *   `-----BEGIN CERTIFICATE-----`/`-----END CERTIFICATE-----`

Example of Grist (behind a reverse-proxy):

```plaintext
URL="https://<grist-host>"
SAML="https://<keycloak-host>/realms/<realm>/protocol/saml"
docker run -d --name=grist \
  -e GRIST_SINGLE_ORG=docs \
  -e APP_HOME_URL="${URL}" \
  -e APP_DOC_URL="${URL}" \
  -e GRIST_SAML_SP_HOST="${URL}" \
  -e GRIST_SAML_SP_KEY=/persist/saml/client.key \
  -e GRIST_SAML_SP_CERT=/persist/saml/client.crt \
  -e GRIST_SAML_IDP_LOGIN="${SAML}" \
  -e GRIST_SAML_IDP_LOGOUT="${SAML}" \
  -e GRIST_SAML_IDP_CERTS=/persist/saml/idp.crt \
  -e GRIST_SAML_IDP_UNENCRYPTED=1 \
  -v grist-data:/persist \
  -p 8484:8484 \
  gristlabs/grist
```

For more information see here:
https://github.com/gristlabs/grist-core/issues/44



### Environment

The environment variables should be adjusted in the docker-compose file.
