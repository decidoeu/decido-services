# DECIDO backend

## Docker Compose

```console
docker-compose up
```

## Variables

The backend needs to be integrated with the airflow service. Please adjust the following variables:

```
- AIRFLOW_URL=https://airflow.domain.tld
- AIRFLOW_USER=airflow_USER
- AIRFLOW_PASSWORD=airflow_user_PASSWORD
- AIRFLOW_AUTHORIZATION_HEADER_KEY=Authorization
- AIRFLOW_AUTHORIZATION_HEADER_VALUE=Basic dXNlci1uYW1lOnNlY3VyZS1wYXNzd29yZA==

# The header value is base 64 encoded string in the format:Basic [encoded string=user-name:secure-password]
# To encode the string you can use:
# https://www.base64encode.org/
# Example:
# Basic dXNlci1uYW1lOnNlY3VyZS1wYXNzd29yZA==
```


## Futher information

Please refer to the README of the DECIDO-Backend repository:

https://gitlab.com/decidoeu/decido-portal-backend-2
