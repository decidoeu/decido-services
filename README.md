# DECIDO services

This repository contains the necessary information to deploy the services needed to deploy the  DECIDO Platform.
Please note that other services such as an SMTP server and a HTTP proxy are also needed.

The order in which the services are deployed does not matter.

This repository has instructions to deploy the following services:

- Airflow
- Discourse
- Grafana
- Grist
- Piveau
- DECIDO-backend
- DECIDO-UI
