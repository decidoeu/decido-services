# DECIDO UI


## Docker Compose

The DECIDO UI can be deployed with the docker-compose file attached.

```console
docker-compose up
```

## Further information.

Please refer to the readme of the DECIDO-UI repository:

https://gitlab.com/decidoeu/decido-portal-ui-2

The decido UI is based on the Angular framework. This framework does not easily allow to change variables in runtime. Therefore the easiest way to change certain parameters, for example URLs, is to change the source code and build a new docker image.
