# DECIDO Specifics

To run the stack execute:

```console
docker-compose up -d
```

## Environment variables

- Change the API keys and store them safely. You will need them to integrate piveau with airflow.

- Configure the OIDC data, for example PIVEAU_HUB_AUTHORIZATION_PROCESS_DATA, PIVEAU_STORE_AUTH_CONFIG and all the VUE_APP_AUTHENTICATION_ variables.

## Integration with keycloak

Follow very carefully these instructions:

https://doc.piveau.eu/hub/integration-keycloak/
